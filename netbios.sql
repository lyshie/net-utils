CREATE TABLE netbios (
         id INTEGER PRIMARY KEY AUTOINCREMENT,
         ip TEXT NOT NULL,
        mac TEXT NOT NULL,
    netbios TEXT,
     server TEXT,
     'user' TEXT,
         os TEXT,
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
);
