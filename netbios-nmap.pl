#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: netbios-nmap.pl
#
#        USAGE: nmap --script smb-os-discovery -p 445 163.26.68.0/24 | ./netbios-nmap.pl
#
#  DESCRIPTION: Parse 'nmap' results
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: SHIE, Li-Yi (lyshie), lyshie@tn.edu.tw
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 2016-07-06 20:19:00
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use DBI;
use FindBin qw($Bin);

sub main {

    # prepare SQL statement
    my $dbh = DBI->connect( "dbi:SQLite:dbname=$Bin/ipmac.db",
        "", "", { AutoCommit => 0, RaiseError => 1, } )
      or die $DBI::errstr;

    my $sth = $dbh->prepare(
        q{
        INSERT OR REPLACE INTO netbios
                               (
                                   id
                                   , ip
                                   , mac
                                   , netbios
                                   , os
                               )
                        VALUES (
                                   (SELECT id FROM netbios WHERE ip = ? AND mac = ?)
                                   , ?
                                   , ?
                                   , ?
                                   , ?
                               )
        }
    );

    my $ip      = '';
    my $mac     = '';
    my $netbios = '';
    my $os      = '';

    while ( my $line = <STDIN> ) {
        chomp($line);

        if ( $line =~ m/^Nmap scan report for.+?(\d+\.\d+\.\d+\.\d+)/ ) {
            $ip = $1;

            # reset to default
            $mac     = '';
            $netbios = '';
            $os      = '';
        }

        if ( $line =~ m/^MAC Address:\s+(\S+)/ ) {
            $mac = $1;
            $mac =~ s/:/\-/g;
            $mac = uc($mac);
        }

        if ( $line =~ m/NetBIOS computer name:\s+(.+)/ ) {
            $netbios = $1;
        }

        if ( $line =~ m/OS:\s+(.+)/ ) {
            $os = $1;
        }

        if ( $netbios ne '' ) {
            print "[$ip][$mac][$netbios][$os]\n";

            # insert into database
            $sth->bind_param( 1, $ip );
            $sth->bind_param( 2, $mac );
            $sth->bind_param( 3, $ip );
            $sth->bind_param( 4, $mac );
            $sth->bind_param( 5, $netbios );
            $sth->bind_param( 6, $os );

            $sth->execute();

            # reset to default
            $mac     = '';
            $netbios = '';
            $os      = '';
        }
    }

    $sth->finish();
    $dbh->commit() or die $DBI::errstr;
    $dbh->disconnect();
}

main;
