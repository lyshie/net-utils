CREATE TABLE ipdesc (
         id INTEGER PRIMARY KEY AUTOINCREMENT, 
         ip TEXT NOT NULL,
      desc1 TEXT, 
      desc2 TEXT, 
      desc3 TEXT, 
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
);
