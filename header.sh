#!/bin/sh

cat <<EOF
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="dokuwiki/lib/tpl/bootstrap3/images/favicon.ico" />
    <link rel="apple-touch-icon" href="dokuwiki/lib/tpl/bootstrap3/images/apple-touch-icon.png" />
    <link type="text/css" rel="stylesheet" href="dokuwiki/lib/tpl/bootstrap3/assets/bootswatch/spacelab/bootstrap.min.css" />

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>

<!--
    <link type="text/css" rel="stylesheet" href="dokuwiki/lib/tpl/bootstrap3/assets/font-awesome/css/font-awesome.min.css" />
-->
    <script type="text/javascript" src="dokuwiki/lib/tpl/bootstrap3/assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.26.5/css/theme.bootstrap_2.min.css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.26.5/js/jquery.tablesorter.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.26.5/js/jquery.tablesorter.widgets.min.js"></script>
</head>
<body>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            // add '<thead>' to table
            var th = \$('tr').has('th');
            \$('table').prepend('<thead></thead>');
            \$('thead').prepend(\$(th));

            var jobs = [];
            \$('table tbody tr').each(function() {
                var obj = \$(this).find('td:nth-child(4)');
                if (obj) {
                    var org = \$(obj).text() || '';
                    if (org == '') {
                        var mac = \$(this).find('td:nth-child(2)').text() || '';
                        jobs.push(
                            \$.get('vendorname.php?mac=' + mac, function(data) {
                                \$(obj).html('<i>' + data + '</i>');
                            })
                        );
                    }
                }
            });

            // when all jobs done
            \$.when.apply(\$, jobs).done(function() {
                alert('ok');
                \$("#ipmac").tablesorter();
            });
        });
    </script>
EOF

date

cat <<EOF
<table id="ipmac" border="1" class="tablesorter">
EOF
