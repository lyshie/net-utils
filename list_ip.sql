.load './libsqliteipv4.so'
.header on
.mode html

SELECT
  m.ip AS ip,
  m.mac,
  DATETIME(MAX(m.timestamp), 'localtime') AS 'time',
  MAX(org) AS org,
  netbios,
  desc2 || ' ' || desc1 AS 'desc',
  os,
  GROUP_CONCAT(p.sysname) AS sysname,
  GROUP_CONCAT(p.port) AS port
FROM
  (SELECT ip, mac, MAX(org) AS org, MAX(timestamp) AS timestamp FROM ipmac GROUP BY ip, mac) m
LEFT OUTER JOIN
  ipdesc d
ON
  m.ip = d.ip
LEFT OUTER JOIN
  netbios n
ON
  (m.ip = n.ip AND m.mac = n.mac)
  OR
  (m.ip = n.ip AND n.mac = '00-00-00-00-00-00')
LEFT OUTER JOIN
  macport p
ON
  (m.mac = p.mac AND p.uplink = 0)
GROUP BY
  m.ip,
  m.mac
ORDER BY
  IP2INT(m.ip);
