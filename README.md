# net-utils
Command line utilities for network administration.

#### 1. Create empty database
We have four relation tables, each of them is maintained by a script.
  1. `IP ~ MAC`

    ```
    # ./arp-scan.py '192.168.0.*' '192.168.1.*'
    ```

  2. `IP ~ Description`

    Find all host declarations in dhcpd config file. Parse _comments_, _hardware ethernet_ and _fixed-address_.
    ```
    # grep -h -B2 -P 'host \d+_\d+' "${DHCPD_CONF_FILE}" | ./ipdesc.pl
    ```
    
    _sample dhcpd config file_
    ```
    # example.edu.tw (example host)
    # HP DL385G8
    host 1_1 {
      hardware ethernet 00:00:d8:b7:20:a6;
       192.168.1.1;
    }
    ```

  3. `(IP, MAC) ~ NetBIOS`

    Use `nbtscan` and `nmap` to find computer names in a LAN.
    ```
    # nbtscan -q -r -s : 192.168.0.0-100   | ./netbios.pl
    # nmap --script smb-os-discovery -p 445 192.168.0.0/24 | ./netbios-nmap.pl
    ```
    
  4. `MAC ~ (Switch, Port)`

    Use `snmpwalk` and `snmpget` to retrieve port/mac pair from a switch.
    
    _Switch: ZyXEL GS-1900-24_
    ```
    # ./snmp-gs1900.sh  10.0.0.2 | ./dgs1210.pl
    ```

    _Switch: D-Link DGS-1210-16_
    ```
    # ./snmp-dgs1210.sh 10.0.0.7 | ./dgs1210.pl
    ```

#### 2. Add all jobs to `Cron`
  ```
  # vim /etc/cron.d/arp-scan
  ```
  
  * /etc/cron.d/arp-scan

  ```
  # daily update host information
  49 23 * * * root /usr/bin/grep -h -B2 -P 'host \d+_\d+' /tmp/dhcpd.conf | /root/net-utils/ipdesc.pl >/dev/null 2>&1

  # periodically scan local network
  */3 * * * * root /root/net-utils/arp-scan.py '192.168.0.*' '192.168.1.*' >/dev/null 2>&1  
  */3 * * * * root /root/net-utils/netbios-scan.sh                         >/dev/null 2>&1
  */3 * * * * root /root/net-utils/snmp/macport-scan.sh                    >/dev/null 2>&1
  ```

#### 3. Create reports
  * All
  ```
  # (./header.sh; sqlite3 ./ipmac.db < ./list_ip.sql; ./footer.sh) > /tmp/all.html
  ```
  
  * Today
  ```
  # (./header.sh; sqlite3 ./ipmac.db < ./list_ip_today.sql; ./footer.sh) > /tmp/today.html
  ```

  * Last 5 minutes
  ```
  # (./header.sh; sqlite3 ./ipmac.db < ./list_ip_5min.sql; ./footer.sh) > /tmp/5min.html
  ```
  
#### 4. Extensions
Use [`ipv4-ext`](http://sqlite.mobigroup.ru/wiki?name=ext_inet) SQLite3 extension to sort IPv4 addresses.
