#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: netbios.pl
#
#        USAGE: nbtscan -r -s : 163.26.68.0/24 | ./netbios.pl
#
#  DESCRIPTION: Parse 'nbtscan' results
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: SHIE, Li-Yi (lyshie), lyshie@tn.edu.tw
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 2016-07-05 19:54:00
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use DBI;
use FindBin qw($Bin);

sub main {

    # prepare SQL statement
    my $dbh = DBI->connect( "dbi:SQLite:dbname=$Bin/ipmac.db",
        "", "", { AutoCommit => 0, RaiseError => 1, } )
      or die $DBI::errstr;

    my $sth = $dbh->prepare(
        q{
        INSERT OR REPLACE INTO netbios
                               (
                                   id
                                   , ip
                                   , mac
                                   , netbios
                                   , server
                                   , 'user'
                               )
                        VALUES (
                                   (SELECT id FROM netbios WHERE ip = ? AND mac = ?)
                                   , ?
                                   , ?
                                   , ?
                                   , ?
                                   , ?
                               )
        }
    );

    while ( my $line = <STDIN> ) {
        chomp($line);
        my @chunks = split( /\s*:\s*/, $line );

        my $ip      = $chunks[0] // '';
        my $netbios = $chunks[1] // '';
        my $server  = $chunks[2] // '';
        my $user    = $chunks[3] // '';
        my $mac     = $chunks[4] // '';

        $mac = uc($mac);

        print "[$ip][$mac][$netbios][$server][$user]\n";

        # insert into database
        $sth->bind_param( 1, $ip );
        $sth->bind_param( 2, $mac );
        $sth->bind_param( 3, $ip );
        $sth->bind_param( 4, $mac );
        $sth->bind_param( 5, $netbios );
        $sth->bind_param( 6, $server );
        $sth->bind_param( 7, $user );

        $sth->execute();
    }

    $sth->finish();
    $dbh->commit() or die $DBI::errstr;
    $dbh->disconnect();
}

main;
