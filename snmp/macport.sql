CREATE TABLE macport (
         id INTEGER PRIMARY KEY AUTOINCREMENT, 
        mac TEXT NOT NULL,
       port INTEGER NOT NULL,
     switch TEXT NOT NULL, 
    sysname TEXT,
     sysloc TEXT,
     uplink INTEGER DEFAULT 0,
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
);
