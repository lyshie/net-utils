#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: gs1900.pl
#
#        USAGE: ./snmp-gs1900.sh "${SWITCH_IP}" | ./gs1900.pl
#
#  DESCRIPTION: Parse ZyXEL GS1900-24 SNMP information
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: SHIE, Li-Yi (lyshie), lyshie@tn.edu.tw
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 2016-07-07 16:56:00
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use FindBin qw($Bin);
use DBI;

my $PREFIX_SWITCH  = 'SWITCH';
my $PREFIX_SYSNAME = 'SNMPv2-MIB::sysName.0';
my $PREFIX_SYSLOC  = 'SNMPv2-MIB::sysLocation.0';

my $PREFIX_MACPORT = 'SNMPv2-SMI::mib-2.17.7.1.2.2.1';
my %TABLES         = ();
my @COUNTS         = ();

sub proc_value {
    my ($value) = @_;
    $value =~ s/(^\s+|\s+$)//g;

    my $result = '';

    if ( $value =~ m/^Hex-STRING:\s*(.+)/ ) {

        # Hex-STRING: 00 1D D8 B7 20 A6
        $result = $1;
        $result =~ s/\s/\-/g;
    }
    elsif ( $value =~ m/^INTEGER:\s*(\d+)/ ) {

        # INTEGER: 3
        $result = $1;
    }
    else {

        # STRING: GS1900-24
        my ( $k, $v ) = split( /\s*:\s*/, $value, 2 );
        $result = $v;
    }

    return $result;
}

sub main {
    my $switch  = '';
    my $sysname = '';
    my $sysloc  = '';

    while ( my $line = <STDIN> ) {
        chomp($line);

        # parse 'snmpget' and 'snmpwalk' results
        if ( $line =~ m/^\Q$PREFIX_SWITCH\E/ ) {
            my ( $oid, $value ) = split( /\s+=\s+/, $line, 2 );
            $switch = proc_value($value);
        }
        elsif ( $line =~ m/^\Q$PREFIX_SYSNAME\E/ ) {
            my ( $oid, $value ) = split( /\s+=\s+/, $line, 2 );
            $sysname = proc_value($value);
        }
        elsif ( $line =~ m/^\Q$PREFIX_SYSLOC\E/ ) {
            my ( $oid, $value ) = split( /\s+=\s+/, $line, 2 );
            $sysloc = proc_value($value);
        }
        elsif ( $line =~ m/^\Q$PREFIX_MACPORT\E\.(.+)/ ) {
            my ( $t, $oid ) = split( /\./, $1, 2 );

            my ( $key, $value ) = split( /\s+=\s+/, $oid, 2 );
            $value = proc_value($value);

            #print "[$t][$key][$value]\n";

            if ( $t == 1 ) {
                $TABLES{$key}{'mac'} = $value;
            }
            elsif ( $t == 2 ) {
                $TABLES{$key}{'port'} = $value;

                $COUNTS[$value] = 0 if ( !defined( $COUNTS[$value] ) );
                $COUNTS[$value]++;
            }
        }
    }

    # prepare SQL statement
    my $dbh = DBI->connect( "dbi:SQLite:dbname=$Bin/ipmac.db",
        "", "", { AutoCommit => 0, RaiseError => 1, } )
      or die $DBI::errstr;

    my $sth = $dbh->prepare(
        q{
        INSERT OR REPLACE INTO macport
                               (
                                   id
                                   , mac
                                   , port
                                   , switch
                                   , sysname
                                   , sysloc
                                   , uplink
                               )
                        VALUES (
                                   (SELECT id FROM macport WHERE mac = ? AND port = ? AND switch = ?)
                                   , ?
                                   , ?
                                   , ?
                                   , ?
                                   , ?
                                   , ?
                               )
        }
    );

    # find uplink port (max number of MACs)
    my $uplink = '';
    my $max    = 0;

    for ( my $i = 0 ; $i < scalar(@COUNTS) ; $i++ ) {
        if ( ( $COUNTS[$i] // 0 ) > $max ) {
            $max    = $COUNTS[$i];
            $uplink = $i;
        }
    }

    # dump all table into database
    foreach my $key ( keys(%TABLES) ) {
        my $mac  = $TABLES{$key}{'mac'};
        my $port = $TABLES{$key}{'port'};

        printf( "[%s][%s][%s][%s][%s]\n",
            $switch, $sysname, $sysloc, $mac, $port );

        # insert into database
        $sth->bind_param( 1, $mac );
        $sth->bind_param( 2, $port );
        $sth->bind_param( 3, $switch );

        $sth->bind_param( 4, $mac );
        $sth->bind_param( 5, $port );
        $sth->bind_param( 6, $switch );
        $sth->bind_param( 7, $sysname );
        $sth->bind_param( 8, $sysloc );
        $sth->bind_param( 9, ( $port eq $uplink ) ? 1 : 0 );

        $sth->execute();
    }

    $sth->finish();
    $dbh->commit() or die $DBI::errstr;
    $dbh->disconnect();
}

main;
