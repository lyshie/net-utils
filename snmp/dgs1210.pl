#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: dgs1210.pl
#
#        USAGE: ./snmp-dgs1210.sh "${SWITCH_IP}" | ./dgs1210.pl
#
#  DESCRIPTION: Parse D-Link DGS-1210-28 SNMP information
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: SHIE, Li-Yi (lyshie), lyshie@tn.edu.tw
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 2016-07-07 22:45:00
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use FindBin qw($Bin);
use DBI;
use Data::Dump;

my $PREFIX_SWITCH  = 'SWITCH';
my $PREFIX_SYSNAME = 'SNMPv2-MIB::sysName.0';
my $PREFIX_SYSLOC  = 'SNMPv2-MIB::sysLocation.0';

my $PREFIX_MACPORT = 'SNMPv2-SMI::mib-2.17.7.1.2.2.1.2.1';
my $TABLES         = ();
my @COUNTS         = ();

my $UPLINK = $ARGV[0] // '1';

sub proc_value {
    my ($value) = @_;
    $value =~ s/(^\s+|\s+$)//g;

    my $result = '';

    if ( $value =~ m/^Hex-STRING:\s*(.+)/ ) {

        # Hex-STRING: 00 1D D8 B7 20 A6
        $result = $1;
        $result =~ s/\s/\-/g;
    }
    elsif ( $value =~ m/^INTEGER:\s*(\d+)/ ) {

        # INTEGER: 3
        $result = $1;
    }
    else {

        # STRING: GS1900-24
        my ( $k, $v ) = split( /\s*:\s*/, $value, 2 );
        $result = $v;
    }

    return $result;
}

sub to_hex_string {
    my ($text) = @_;

    my $hex_str = join( "-",
        map { sprintf( "%02X", $_ ) } ( split( /\./, $text ) )[ -6 .. -1 ] );

    return $hex_str;
}

sub main {
    my $switch  = '';
    my $sysname = '';
    my $sysloc  = '';

    while ( my $line = <STDIN> ) {
        chomp($line);

        # parse 'snmpget' and 'snmpwalk' results
        if ( $line =~ m/^\Q$PREFIX_SWITCH\E/ ) {
            my ( $oid, $value ) = split( /\s+=\s+/, $line, 2 );
            $switch = proc_value($value);
        }
        elsif ( $line =~ m/^\Q$PREFIX_SYSNAME\E/ ) {
            my ( $oid, $value ) = split( /\s+=\s+/, $line, 2 );
            $sysname = proc_value($value);
        }
        elsif ( $line =~ m/^\Q$PREFIX_SYSLOC\E/ ) {
            my ( $oid, $value ) = split( /\s+=\s+/, $line, 2 );
            $sysloc = proc_value($value);
        }

        # ifRcvAddressStatus[26][STRING: 10:be:f5:48:de:85] = INTEGER: active(1)
        elsif ( $line =~ m/^\Q$PREFIX_MACPORT\E\.(.+)\s+=\s+(.+)/ ) {
            my $m = to_hex_string($1);
            my $p = proc_value($2);

            $TABLES->{$p} = [] if ( !defined( $TABLES->{$p} ) );

            push( $TABLES->{$p}, $m );
        }
    }

    print Data::Dump->dump($TABLES), "\n";

    # prepare SQL statement
    my $dbh = DBI->connect( "dbi:SQLite:dbname=$Bin/ipmac.db",
        "", "", { AutoCommit => 0, RaiseError => 1, } )
      or die $DBI::errstr;

    my $sth = $dbh->prepare(
        q{
        INSERT OR REPLACE INTO macport
                               (
                                   id
                                   , mac
                                   , port
                                   , switch
                                   , sysname
                                   , sysloc
                                   , uplink
                               )
                        VALUES (
                                   (SELECT id FROM macport WHERE mac = ? AND port = ? AND switch = ?)
                                   , ?
                                   , ?
                                   , ?
                                   , ?
                                   , ?
                                   , ?
                               )
        }
    );

    # find uplink port (max number of MACs)
    my $uplink = '';
    my $max    = 0;

    foreach my $p ( keys(%$TABLES) ) {
        my $c = scalar( @{ $TABLES->{$p} } );
        if ( $c > $max ) {
            $max    = $c;
            $uplink = $p;
        }
    }

    # dump all table into database
    foreach my $port ( keys(%$TABLES) ) {
        foreach my $mac ( @{ $TABLES->{$port} } ) {

            printf( "[%s][%s][%s][%s][%s]\n",
                $switch, $sysname, $sysloc, $mac, $port );

            # insert into database
            $sth->bind_param( 1, $mac );
            $sth->bind_param( 2, $port );
            $sth->bind_param( 3, $switch );

            $sth->bind_param( 4, $mac );
            $sth->bind_param( 5, $port );
            $sth->bind_param( 6, $switch );
            $sth->bind_param( 7, $sysname );
            $sth->bind_param( 8, $sysloc );

            # TODO: ($port eq $uplink) ? 1 : 0
            $sth->bind_param( 9, ( $port eq $uplink ) ? $UPLINK : 0 );

            $sth->execute();
        }
    }

    $sth->finish();
    $dbh->commit() or die $DBI::errstr;
    $dbh->disconnect();
}

main;
