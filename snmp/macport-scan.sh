#!/bin/sh

curr_dir=`pwd`
dir=`dirname $0`
ABS_PATH=`cd $dir; pwd`

"$ABS_PATH/snmp-gs1900.sh"  10.0.0.2  | "$ABS_PATH/dgs1210.pl"
"$ABS_PATH/snmp-gs1900.sh"  10.0.0.3  | "$ABS_PATH/dgs1210.pl"
"$ABS_PATH/snmp-gs1900.sh"  10.0.0.4  | "$ABS_PATH/dgs1210.pl"
"$ABS_PATH/snmp-gs1900.sh"  10.0.0.5  | "$ABS_PATH/dgs1210.pl"
"$ABS_PATH/snmp-gs1900.sh"  10.0.0.6  | "$ABS_PATH/dgs1210.pl"
"$ABS_PATH/snmp-dgs1210.sh" 10.0.0.7  | "$ABS_PATH/dgs1210.pl"
"$ABS_PATH/snmp-gs1900.sh"  10.0.0.8  | "$ABS_PATH/dgs1210.pl"
"$ABS_PATH/snmp-dgs1210.sh" 10.0.0.9  | "$ABS_PATH/dgs1210.pl"
"$ABS_PATH/snmp-dgs1210.sh" 10.0.0.10 | "$ABS_PATH/dgs1210.pl"
