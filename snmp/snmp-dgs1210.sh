#!/bin/sh

SWITCH=$1

echo "SWITCH = STRING: $1"
snmpget  -v 2c -c public "${SWITCH}" SNMPv2-MIB::sysName.0
snmpget  -v 2c -c public "${SWITCH}" SNMPv2-MIB::sysLocation.0
snmpwalk -v 2c -c public "${SWITCH}" SNMPv2-SMI::mib-2.17.7.1.2.2.1.2.1
