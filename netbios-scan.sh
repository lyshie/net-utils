#!/bin/sh

curr_dir=`pwd`
dir=`dirname $0`
ABS_PATH=`cd $dir; pwd`

nbtscan -q -r -s : 163.26.68.0-100   | iconv -f big5 -t utf8 | "$ABS_PATH/netbios.pl"
nbtscan -q -r -s : 163.26.68.101-150 | iconv -f big5 -t utf8 | "$ABS_PATH/netbios.pl"
nbtscan -q -r -s : 163.26.68.151-255 | iconv -f big5 -t utf8 | "$ABS_PATH/netbios.pl"

nbtscan -q -r -s : 163.26.69.0-100   | iconv -f big5 -t utf8 | "$ABS_PATH/netbios.pl"
nbtscan -q -r -s : 163.26.69.101-150 | iconv -f big5 -t utf8 | "$ABS_PATH/netbios.pl"
nbtscan -q -r -s : 163.26.69.151-255 | iconv -f big5 -t utf8 | "$ABS_PATH/netbios.pl"

nmap --script smb-os-discovery -p 445 163.26.68.0/24 | iconv -f big5 -t utf8 | "$ABS_PATH/netbios-nmap.pl"
nmap --script smb-os-discovery -p 445 163.26.69.0/24 | iconv -f big5 -t utf8 | "$ABS_PATH/netbios-nmap.pl"
