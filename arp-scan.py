#!/usr/bin/env python
# -*- coding: utf-8 -*-

#=========================================================================
#
#         FILE: arp-scan.py
#
#        USAGE: ./arp-scan.py '163.26.68.*' '163.26.69.*'
#
#  DESCRIPTION: Basic ARP scanner
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: SHIE, Li-Yi (lyshie), lyshie@tn.edu.tw
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 2016-06-25 09:06:00
#     REVISION: ---
#=========================================================================


import os
import sys
import sqlite3
from netaddr import *
from scapy.all import Ether, ARP, conf, arping


def main():
    # command line arguments: '163.26.68.*'
    nets = sys.argv[1:]

    # script path
    base_path = os.path.dirname(os.path.abspath(__file__))
    # db filename
    db_file = os.path.join(base_path, 'ipmac.db')

    conn = sqlite3.connect(db_file)
    c = conn.cursor()

    '''begin'''
    conf.verb = 0

    for net in nets:
        ans, unans = arping(net, timeout=2, inter=0.005)
        for sndu, rcv in ans:
            mac, ip = rcv.sprintf(r"%Ether.src%,%ARP.psrc%").split(",")
            eui = EUI(mac)
            org = ''

            # lookup organization
            try:
                oui = eui.oui
                org = oui.registration().org
            except:
                pass

            c.execute(
                "INSERT INTO ipmac (ip, mac, org) VALUES (?, ?, ?)", (ip, str(eui), org))
    '''end'''

    conn.commit()
    conn.close()

if __name__ == '__main__':
    main()
